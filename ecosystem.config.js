module.exports = {
	apps : [{
		name      : 'core/proxy',
		script    : 'build/proxy.js',
		env: {
			// Either inject the values via environment variables or define them here
            PROXY_USE: process.env.PROXY_USE || false,
            PROXY_PORT: process.env.PROXY_PORT || 9000,
            NGROK_USE: process.env.NGROK_USE || false,
            NGROK_SUBDOMAIN: process.env.NGROK_SUBDOMAIN || "",
            NGROK_AUTHTOKEN: process.env.NGROK_AUTHTOKEN || "",
            SLACK_USE_EVENTS_API: process.env.SLACK_USE_EVENTS_API || false,
            SLACK_EVENTS_HOST: process.env.SLACK_EVENTS_HOST || "localhost",
            SLACK_EVENTS_PORT: process.env.SLACK_EVENTS_PORT || 9001,
            SLACK_USE_ACTIONS_API: process.env.SLACK_USE_ACTIONS_API || false,
            SLACK_ACTIONS_HOST: process.env.SLACK_ACTIONS_HOST || "localhost",
            SLACK_ACTIONS_PORT: process.env.SLACK_ACTIONS_PORT || 9002,
            SLACK_USE_COMMANDS_API: process.env.SLACK_USE_COMMANDS_API || false,
            SLACK_COMMANDS_HOST: process.env.SLACK_COMMANDS_HOST || "localhost",
            SLACK_COMMANDS_PORT: process.env.SLACK_COMMANDS_PORT || 9003
		}
	}]
};
