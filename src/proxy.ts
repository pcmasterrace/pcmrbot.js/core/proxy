import * as redbird from "redbird";
import * as yn from "yn";
import * as ngrok from "ngrok";

if (yn(process.env.PROXY_USE, {default: false})) {
	const proxy = redbird({port: Number(process.env.PROXY_PORT), bunyan: false});

	// Register routes
	// Slack API
	if (yn(process.env.SLACK_USE_EVENTS_API, {default: false})) {
		proxy.register(`localhost:${process.env.PROXY_PORT}/slack/events`, `${process.env.SLACK_EVENTS_HOST}:${process.env.SLACK_EVENTS_PORT}`);
		console.log(`Now routing http://localhost:${process.env.PROXY_PORT}/slack/events to http://${process.env.SLACK_EVENTS_HOST}:${process.env.SLACK_EVENTS_PORT}`)
		if (yn(process.env.PROXY_NGROK_USE, {default: false})) {
			proxy.register(`${process.env.PROXY_NGROK_SUBDOMAIN}.ngrok.io/slack/events`, `${process.env.SLACK_EVENTS_HOST}:${process.env.SLACK_EVENTS_PORT}`)
			console.log(`Now routing https://${process.env.PROXY_NGROK_SUBDOMAIN}.ngrok.io/slack/events to http://${process.env.SLACK_EVENTS_HOST}:${process.env.SLACK_EVENTS_PORT}`)
		}
	}
	if (yn(process.env.SLACK_USE_ACTIONS_API, {default: false})) {
		proxy.register(`localhost:${process.env.PROXY_PORT}/slack/actions`, `${process.env.SLACK_ACTIONS_HOST}:${process.env.SLACK_ACTIONS_PORT}`);
		console.log(`Now routing http://localhost:${process.env.PROXY_PORT}/slack/actions to http://${process.env.SLACK_ACTIONS_HOST}:${process.env.SLACK_ACTIONS_PORT}`)
		if (yn(process.env.PROXY_NGROK_USE, {default: false})) {
			proxy.register(`${process.env.PROXY_NGROK_SUBDOMAIN}.ngrok.io/slack/actions`, `${process.env.SLACK_ACTIONS_HOST}:${process.env.SLACK_ACTIONS_PORT}`)
			console.log(`Now routing https://${process.env.PROXY_NGROK_SUBDOMAIN}.ngrok.io/slack/actions to http://${process.env.SLACK_ACTIONS_HOST}:${process.env.SLACK_ACTIONS_PORT}`)
		}
	}
	if (yn(process.env.SLACK_USE_COMMANDS_API, {default: false})) {
		proxy.register(`localhost:${process.env.PROXY_PORT}/slack/commands`, `${process.env.SLACK_COMMANDS_HOST}:${process.env.COMMANDS_PORT}`);
		console.log(`Now routing http://localhost:${process.env.PROXY_PORT}/slack/commands to http://${process.env.SLACK_COMMANDS_HOST}:${process.env.SLACK_COMMANDS_PORT}`)
		if (yn(process.env.PROXY_NGROK_USE, {default: false})) {
			proxy.register(`${process.env.PROXY_NGROK_SUBDOMAIN}.ngrok.io/slack/commands`, `${process.env.SLACK_COMMANDS_HOST}:${process.env.SLACK_COMMANDS_PORT}`)
			console.log(`Now routing https://${process.env.PROXY_NGROK_SUBDOMAIN}.ngrok.io/slack/commands to http://${process.env.SLACK_COMMANDS_HOST}:${process.env.SLACK_COMMANDS_PORT}`)
		}
	}

	// Facebook API
	if (yn(process.env.BOT_HIJACKDETECTOR_USE, {default: false})) {
		proxy.register(`localhost:${process.env.PROXY_PORT}/facebook`, `${process.env.BOT_HIJACKDETECTOR_HOST}:${process.env.BOT_HIJACKDETECTOR_PORT}`);
		console.log(`Now routing http://localhost:${process.env.PROXY_PORT}/facebook to http://${process.env.BOT_HIJACKDETECTOR_HOST}:${process.env.BOT_HIJACKDETECTOR_PORT}`)
		if (yn(process.env.PROXY_NGROK_USE, {default: false})) {
			proxy.register(`${process.env.PROXY_NGROK_SUBDOMAIN}.ngrok.io/facebook`, `${process.env.BOT_HIJACKDETECTOR_HOST}:${process.env.BOT_HIJACKDETECTOR_PORT}`)
			console.log(`Now routing https://${process.env.PROXY_NGROK_SUBDOMAIN}.ngrok.io/facebook to http://${process.env.BOT_HIJACKDETECTOR_HOST}:${process.env.BOT_HIJACKDETECTOR_PORT}`)
		}
	}

	// ngrok configuration
	if (yn(process.env.PROXY_NGROK_USE, {default: false})) {
		ngrok.connect({
			addr: Number(process.env.PROXY_PORT),
			authtoken: process.env.NGORK_AUTHTOKEN,
			subdomain: process.env.PROXY_NGROK_SUBDOMAIN
		}).then(() => console.log(`ngrok tunnel at https://${process.env.PROXY_NGROK_SUBDOMAIN}.ngrok.io is now active`))
	}
}